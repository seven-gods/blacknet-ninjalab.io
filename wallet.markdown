---
titleOnlyInHead: 1
---

Prerequisite: <i class="fab fa-java"></i> Java 8 or higher ([OpenJDK](https://openjdk.java.net/install/), [Oracle Java](https://java.com/download/), or other).

* [Download 0.2.3](https://vasin.nl/blacknet-0.2.3.zip) and unzip the latest release.
* Change directory to `blacknet/bin`
* On Windows run `blacknet.bat`
* On UN*X run `./blacknet`

Web interface is available at [http://localhost:8283/](http://localhost:8283/). Also see [HTTP API](apiv1.html).

BLAKE2b-256 Hash: b8c28df6d5fb58fc0874accae08832fd9363d95076400ba9cb6aa9af60ced91a
