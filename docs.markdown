---
titleOnlyInHead: 1
---
## Docs

- [HTTP API](apiv1.html)
- [Anonymous connection to network](anonnet.html)
- [Initial distribution](burn.html)

## Development
To build and run development version:
```
git clone https://gitlab.com/blacknet-ninja/blacknet.git
cd blacknet
./gradlew run
```