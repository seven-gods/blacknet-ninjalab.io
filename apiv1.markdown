---
title: API Documentation
---

API version 1

API web-server listens on [http://localhost:8283/](http://localhost:8283/) It can be configured in config/rpc.conf

Path of all methods is prefixed with `/api/v1`

Encoding of values |
--- | ---
amount | [satoshis](https://en.bitcoin.it/wiki/Satoshi_(unit)) (1 coin = 100000000 satoshi)
data | JSON
hash | HEX-string
boolean | string "true" or "false"
string | UTF-8 string

Example of GET request: [http://localhost:8283/api/v1/nodeinfo](http://localhost:8283/api/v1/nodeinfo)

- Peers |
--- | ---
Type | GET
Path | /peerinfo
Returns data about each connected network node.

- Node info |
--- | ---
Type | GET
Path | /nodeinfo
Returns data about this node.

- Get block |
--- | ---
v2 | changed format of returned data
Type | GET
Path | /blockdb/get/{hash}/{txdetail?}
Arguments |
hash | hash of block
txdetail | true to include transaction data (optional boolean, default false)
Returns data of a block with given block-hash.

- Get block index |
--- | ---
Type | GET
Path | /blockdb/getblockindex/{hash}
Arguments |
hash | hash of block
Returns index of a block with given block-hash.

- Get block hash |
--- | ---
Type | GET
Path | /blockdb/getblockhash/{height}
Arguments |
height | height of block
Returns hash of a block with given block-height.

- Ledger |
--- | ---
Type | GET
Path | /ledger
Returns data about current state of ledger.

- Account info |
--- | ---
Type | GET
Path | /ledger/get/{account}/{confirmations?}
Arguments |
account | address of account
confirmations | number of confirmations for confirmed balance (optional, default 10)
Returns data of an account with given address.

- Transaction pool |
--- | ---
Type | GET
Path | /txpool
Returns data about memory pool of transactions.

- New account |
--- | ---
Type | GET
Path | /account/generate
Returns data of new account. Save mnemonic before using account.

- Address info |
--- | ---
Type | GET
Path | /address/info/{address}
Arguments |
address | address of account
Returns data about address: public key.

- Mnemonic info |
--- | ---
Type | POST
Path | /mnemonic/info/{mnemonic}
Arguments |
mnemonic | mnemonic of account
Returns data about mnemonic: address and public key.

- Transfer |
--- | ---
Type | POST
Path | /transfer/{mnemonic}/{fee}/{amount}/{to}/{message?}/{encrypted?}
Arguments |
mnemonic | mnemonic of sending account
fee | transaction fee
amount | amount to transfer
to | address of receiving account
message | text message (optional)
encrypted | 0 for plain message, 1 for encrypted (optional, default 0)
Returns transaction hash.

- Burn |
--- | ---
Type | POST
Path | /burn/{mnemonic}/{fee}/{amount}/{message?}
Arguments |
mnemonic | mnemonic of sending account
fee | transaction fee
amount | amount to burn
message | HEX-encoded message (optional)
Returns transaction hash.

- Lease |
--- | ---
Type | POST
Path | /lease/{mnemonic}/{fee}/{amount}/{to}
Arguments |
mnemonic | mnemonic of sending account
fee | transaction fee
amount | amount to lease
to | address of receiving account
Returns transaction hash.

- Cancel lease |
--- | ---
Type | POST
Path | /cancellease/{mnemonic}/{fee}/{amount}/{to}/{height}
Arguments |
mnemonic | mnemonic of sending account
fee | transaction fee
amount | leased amount
to | address of receiving account
height | height of lease
Returns transaction hash.

- Send raw transaction |
--- | ---
Type | GET
Path | /transaction/raw/send/{serialized}
Arguments |
serialized | HEX-encoded transaction
Returns transaction hash.

- Decrypt message |
--- | ---
Type | POST
Path | /decryptmessage/{mnemonic}/{from}/{message}
Arguments |
mnemonic | mnemonic of receiver
from | address of sender
message | encrypted message
Returns string.

- Sign message |
--- | ---
Type | POST
Path | /signmessage/{mnemonic}/{message}
Arguments |
mnemonic | mnemonic of signing account
message | text message
Returns signature.

- Verify message |
--- | ---
Type | GET
Path | /verifymessage/{account}/{signature}/{message}
Arguments |
account | address of signing account
signature | signature to verify
message | text message
Returns boolean.

- Add peer |
--- | ---
Type | GET
Path | /addpeer/{address}/{port?}
Arguments |
address | network address
port | network port (optional, default 28453)
force | force connection (optional boolean, default false)
Returns string.

- Start staking |
--- | ---
Type | POST
Path | /startStaking/{mnemonic}
Arguments |
mnemonic | mnemonic of staking account
Returns boolean.

- Stop staking |
--- | ---
Type | POST
Path | /stopStaking/{mnemonic}
Arguments |
mnemonic | mnemonic of staking account
Returns boolean.
